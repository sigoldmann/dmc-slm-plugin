# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 18:01:52 2024

@author: Simon Goldmann
"""

import numpy as np
import screeninfo
import cv2
import matplotlib.pyplot as plt

class Controller:
    
    def __init__(self, screen_id, masks_dir, correctionFile, val2pi):
        # Define the used constants for the SLM
        self.screen_id = screen_id
        self.mask_dir = masks_dir
        self.correction_mask = cv2.imread(correctionFile)   # .bmp
        self.value_2pi = val2pi
        self.window_name = "slm_" + str(screen_id)
        
        # Getting the size of the screen and coordinates
        screen = screeninfo.get_monitors()[self.screen_id]
        self.width, self.height, self.x, self.y = screen.width, screen.height, screen.x, screen.y
        
        # Define black (0) and white image (255)
        self.black_image = np.zeros((self.height, self.width), dtype=np.uint8)
        self.white_image = np.ones((self.height, self.width), dtype=np.uint8) * 255
        
        # Setting the window to fullscreen on the desired screen
        cv2.namedWindow(self.window_name, cv2.WINDOW_NORMAL)
        cv2.moveWindow(self.window_name, self.x - 1, self.y - 1)
        cv2.setWindowProperty(self.window_name, cv2.WND_PROP_FULLSCREEN,
                              cv2.WINDOW_FULLSCREEN)
        
        # Set the SLM initially to show a black image
        self.showBlackImage()
        
    def destroy(self):
        cv2.destroyAllWindows()

###############################################################################
# Basic methods to project the desired image onto the SLM
    def loadPhaseMask(self, filename, flipMode=None):
        path = self.mask_dir + filename
        mask = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        # Apply image shaping
        if (flipMode is not None):
            mask = cv2.flip(mask, flipMode)
        self.displayMask(mask)
    
    def displayMask(self, mask):
        # Convert .npy in .bmp
        self.current_phase_mask = mask
        mask = mask * 255/(2*np.pi)
        
        self.current_bit_mask = mask
        
        # Add correction file
        #corrected_mask = mask + self.correction_mask   # ???
        corrected_mask = mask
        wrapped_mask = corrected_mask % 256
        self.current_corrected_mask = corrected_mask
        display_mask = wrapped_mask * self.value_2pi / 255
        self.current_scaled_mask = display_mask
        cv2.imshow(self.window_name, display_mask)
        cv2.waitKey(1)
        
###############################################################################
# Basic methods to set a test/simple image on the SLM

    def showBlackImage(self):
        self.displayMask(self.black_image)
        
    def showWhiteImage(self):
        self.displayMask(self.white_image)
        
###############################################################################
# More complex methods implementing arbitrary patterns

    def grating(self):
        pass

###############################################################################
# Methods to visualize displayed masks
    
    def plotMask(self):
        fig, ax = plt.subplots(nrows=2, ncols=2)
        ax[0, 0].set_title("Phase mask")
        ax[0, 0].imshow(self.current_phase_mask)
        ax[0, 1].set_title("Bit mask")
        ax[0, 1].imshow(self.current_bit_mask)
        ax[1, 0].set_title("Corrected mask")
        ax[1, 0].imshow(self.current_corrected_mask)
        ax[1, 1].set_title("Scaled mask")
        im = ax[1, 1].imshow(self.current_scaled_mask)
        
        fig.suptitle("SLM (screen id: %d)" % self.screen_id)
        fig.colorbar(im, ax=ax.ravel().tolist(), orientation="vertical")
        #fig.tight_layout()

###############################################################################
# Methods interacting with the file system

    def setMaskDir(self, directory):
        self.mask_dir = directory