# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 19:17:41 2024

@author: Simon Goldmann
"""

import screeninfo

class Screens:
    
    def listScreens(verbose=False):
        screen_list = screeninfo.get_monitors()
        if (verbose):
            print(100*"-")
            for i, screen in enumerate(screen_list):
                print("ID: %2d | %s" % (i, screen))
                print(100*"-")
        print()
        return screen_list