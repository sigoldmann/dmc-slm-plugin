# -*- coding: utf-8 -*-
"""
Created on Sun Mar 10 22:48:10 2024

@author: Simon Goldmann
"""

import serial

'''
Short implementation of a class which can be used to connect to a serial port
and waits for incoming commands to be used with other software over a virtualized
serial cable or receiving commands from a device
(Useful for connecting python scripts to services like Direct Machine Control)
'''
class Connector:
    
    '''
    Initializes Connector-object by setting the serial port to listen on
    '''
    def __init__(self, port, eol_sign=";"):
        self.serial_port = port
        self.eol = eol_sign
        
    '''
    Reads the incoming serial stream and waits until the end of line (eol)
    character (default ';') is read. Everything received between two eol chars
    is then returned as a string
    Caution: Method blocks the script until a eol char is read
    Future: Add an optional timeout to be used when necessary
    '''
    def waitForCommand(self):
        with serial.Serial(self.serial_port) as ser:
            command = ""
            while (True):
                char = ser.read().decode("utf-8")
                if (char == self.eol):
                    break
                else:
                    command += char
        return command
    
    '''
    Takes a command and splits it at the given delimeter (default '_') and
    returns the substrings as a list
    '''
    def splitCommand(self, command, delim="_"):
        array = command.split(delim)
        return array