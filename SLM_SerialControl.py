# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 18:18:22 2024

@author: Simon Goldmann
"""

import SLM
import DMC
from HardwareUtils import Screens

import sys

serial_port = "COM5"

masks_dir = "masks/"
corrections_dir = "corrections/"
settings_dir = "settings/"

settings_file = "id_to_slm.txt"
correction_file_0 = "CAL_LSH0904189_1030nm.bmp"
correction_file_1 = "CAL_LSH0904190_1030nm.bmp"

def readSettingsFile(filename):
    with open(settings_dir + filename, "r") as file:
        lines = file.readlines()
        return lines
    
def writeToSettingsFile(filename, lineNumber, text):
    with open(settings_dir + filename, "r") as file:
        lines = file.readlines()
    lines[lineNumber] = text + "\n"
    with open(settings_dir + filename, "w") as file:
        file.writelines(lines)
    
def screenChoice():
    screens = Screens.listScreens(verbose=True)
    if (len(screens) < 2):
        print("[ERROR] Not enough screens for 2 SLMs available!")
        sys.exit(0)
    slm_settings = readSettingsFile(settings_file)
    print("Current identifiers for SLMs to be used: ")
    for s in slm_settings:
        print(s)    
    
    slm_0_id = int(slm_settings[0].split(":")[1])
    slm_1_id = int(slm_settings[1].split(":")[1])
    
    if (input("Are these settings still correct? (Refer to the table above) [Y/n]: ") == "n"):
        slm_0_id = int(input("Enter new id for slm_0: "))
        if (slm_0_id > ((len(screens) -1))):
            print("ID not available, please start again")
            sys.exit(0)
         
        slm_1_id = int(input("Enter new id for slm_1: "))
        if (slm_0_id > ((len(screens) -1)) or slm_0_id == slm_1_id):
            print("ID not available, please start again")
            sys.exit(0)
        
        if (input("Save? [Y/n]: ") != "n"):
            writeToSettingsFile(settings_file, 0, "slm_0_id:%d" % slm_0_id)
            writeToSettingsFile(settings_file, 1, "slm_1_id:%d" % slm_1_id)
    print(100*"-")    
    return (slm_0_id, slm_1_id)


conn = DMC.Connector(serial_port)
(slm_id_0, slm_id_1) = screenChoice()

slm_0 = SLM.Controller(slm_id_0, masks_dir, corrections_dir + correction_file_0)
slm_1 = SLM.Controller(slm_id_1, masks_dir, corrections_dir + correction_file_1)

while (True):
    command = conn.waitForCommand()
    print("Received command: " + command, end="", flush=True)
    if (command == "quit"):
        print(" -> Terminating script")
        break
    elif (command.find("_") == -1):
        print(" -> Incomplete command")
    else:
        command_args = conn.splitCommand(command)
        command_keyword = command_args.pop(0)
        slm_number = command_args.pop(0)
        
        if (slm_number == "0"):
            current_slm = slm_0
            other_slm = slm_1
        elif (slm_number == "1"):
            current_slm = slm_1
            other_slm = slm_0
        else:
            print("\n[ERROR] Received unusable slm identifier: ", slm_number)
            continue
        
        if (command_keyword == "test"):
            print(" -> Test for slm_" + slm_number + " successful! Screen ID: ", current_slm.screen_id)
        elif (command_keyword == "white"):
            current_slm.showWhiteImage()
            print(" -> Setting slm_" + slm_number + " to white")
        elif (command_keyword == "black"):
            current_slm.showBlackImage()
            print(" -> Setting slm_" + slm_number + " to black")
        elif (command_keyword == "mask"):
            current_slm.loadPhaseMask(command_args[0])
            print(" -> Setting slm_" + slm_number + " to mask '" + command_args[0] + "'")
        elif (command_keyword == "mirror"):
            current_slm.loadPhaseMask(command_args[0])
            other_slm.loadPhaseMask(command_args[0], flipMode=1)
            print(" -> Setting slm_" + slm_number + " to mask '" + command_args[0] + "'")
            print(" -> Setting other slm to flipped mask '" + command_args[0] + "'")
        else:
            print(" -> Unknown command")
            
slm_0.destroy()
slm_1.destroy()